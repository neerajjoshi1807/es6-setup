function map (arr1, arr2, key){
    const arr = arr1.map((element, index)=>{
        const obj = [element, arr2[index]]
        return obj
    })
    let question_map = new Map(arr)
    if(question_map.has(key)){
        const medals = question_map.get(key);
        question_map.delete(key)
        const size = question_map.size;
        const keys = function (){
            let array1 = []
            for(let x of question_map.keys())
                array1.push(x)
            return array1;
        };
        return {value:medals, size:size, keys:keys(), map:question_map}
    }else{
        let output_string = []
        for(let [key, value] of question_map){
            output_string.push(key + ":" + value)
        }
        return output_string.join("\n")
    }
}
module.exports=map;
