class Shape
{
    constructor(name,sides,sideLength)
    {
        this.name=name;
        this.sides=sides;
        this.sideLength=sideLength;
    }
    calcPerimeter()
    {
        var sum=0;
        for(var i=1;i<=this.sides;i++)
        {
          sum=sum+this.sideLength;  
        }
        return(sum);
    }
}
const Square= new Shape("square",4,5);
var squareperim=Square.calcPerimeter();
const Triangle=new Shape("triangle",3,3);
var triperim=Triangle.calcPerimeter();
module.exports = {
 class: Shape,
 Square: Square,
 Triangle: Triangle,
 SquarePerimeter: squareperim,
 TrianglePerimeter: triperim
 }
